<?php

namespace App\Controller;

use App\Entity\Product;
use App\Form\ProductType;
use App\Repository\ProductRepository;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Service\FileUploader;

/**
 * @Route("/product")
 */
class ProductController extends Controller
{
    /**
     * @Route("/", name="product_index", methods="GET")
     */
    public function index(ProductRepository $productRepository): Response
    {
        return $this->render('product/index.html.twig', ['products' => $productRepository->findAll()]);
    }

    /**
     * @Route("/new", name="product_new", methods="GET|POST")
     */
    public function new(Request $request, FileUploader $fileUploader): Response
    {
        $product = new Product();
        $form = $this->createForm(ProductType::class, $product);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $file = $product->getBrochure();
            $fileName = $fileUploader->upload($file);
            $product->setBrochure($fileName);
            $array=explode(",", $file);
            $em = $this->getDoctrine()->getManager();
            $em->persist($product);
            $em->flush();



            return $this->redirectToRoute('product_index');
        }

        return $this->render('product/new.html.twig', [
            'product' => $product,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @return string
     */
    private function generateUniqueFileName()
    {
        return md5(uniqid());
    }

    /**
     * @Route("/{id}/view", name="product_show", methods="GET")
     */
    public function show( $id): Response
    {   

        $em = $this->getDoctrine()->getManager();
        $product = $em->getRepository(Product::class)->findOneById($id);

        if(isset($product)){

            return $this->render('product/show.html.twig', array('product' => $product));

        }else{
            return new Response('not ok');
        }
    }

    /**
     * @Route("/{id}/edit", name="product_edit", methods="GET|POST")
     */
    public function edit(Request $request, Product $product): Response
    {
        $form = $this->createForm(ProductType::class, $product);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('product_edit', ['id' => $product->getId()]);
        }

        return $this->render('product/edit.html.twig', [
            'product' => $product,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="product_delete", methods="DELETE")
     */
    public function delete(Request $request, Product $product): Response
    {
        if ($this->isCsrfTokenValid('delete'.$product->getId(), $request->request->get('_token'))) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($product);
            $em->flush();
        }

        return $this->redirectToRoute('product_index');
    }
    // /**
    //  * @Route("/export", name="export")
    //  */
    // public function exportAction(){
    //     $em = $this->getDoctrine()->getManager();
    //     $product = $em->getRepository(Product::class)->findAll();
    //     // $writer =$this->container->get(id: 'egyg33k.csv.writer');
    //     $csv = $writer::CreateFromFileObject(new \SplTempFileObject());
    //     $csv->insertOne(['Name', 'Price']);

    //     foreach ($products as $product) {
    //         $csv->insertOne([$product->getName(), $product->getPrice()]);
    //     }

    //     $csv->output('products.csv');
    //     exit;
    // }

    /**
     * @Route("/export", name="product_export")
     */
    public function exportAction(){
    $list = array (
        array(''),
        array('123', '456', '789', '102'),
        array('"aaa"', '"bbb"')
    );
 
    $fp = fopen("sample.csv", "w");
 
    foreach ($list as $line)
    {
        fputcsv(
            $fp, // The file pointer
            $line, // The fields
            ',' // The delimiter
        );
    }
 
    fclose($fp);

        return new Response('Export is good'); }


    /**
     * @Route("/import", name="product_import")
     */
    public function importAction(){
            $rowNo = 1;
        // $fp is file pointer to file sample.csv
    if (($fp = fopen("sample.csv", "r")) !== FALSE) {
        while (($row = fgetcsv($fp, 1000, ",")) !== FALSE) {
            $num = count($row);
            echo "<p> $num fields in line $rowNo: <br /></p>\n";
            $rowNo++;
            for ($c=0; $c < $num; $c++) {
                echo $row[$c] . "<br />\n";
            }
        }
        fclose($fp);
        
            return new Response('Import is good'); 
        }
    }
}
